import React, { useState, useCallback } from 'react';
import cx from 'classnames';
import { useKeyPress } from '../../utils/customHooks';
import { KEY_ENTER, KEY_ESC } from '../../utils/constants';
import style from './style.module.css';

const Task = ({ title, isCompleted, onStatusChange, onDelete, onEdit }) => {
  const empty = '';
  const [isEditable, setEditStatus] = useState(false);
  const [localTitle, setLocalTitle] = useState(empty);

  const handleEditRequest = useCallback(() => {
    setLocalTitle(title);
    setEditStatus(true);
  }, [title]);

  const handleChange = useCallback(({ target: { value } }) => {
    setLocalTitle(value);
  }, []);

  const handleSave = useCallback(() => {
    if (localTitle.length !== 0) {
      onEdit(localTitle);
      setEditStatus(false);
      setLocalTitle(empty);
    }
  }, [localTitle, onEdit]);

  const handleKeys = useKeyPress({
    [KEY_ENTER]: handleSave,
    [KEY_ESC]: () => setEditStatus(false),
  });

  return (
    <div className={style.container}>
      <input
        type="checkbox"
        className={style.checkbox}
        title={`Mark as ${isCompleted ? 'complete' : 'incomplete'}`}
        checked={isCompleted}
        onChange={onStatusChange}
        disabled={isEditable}
      />
      {isEditable ? (
        <React.Fragment>
          <span className={style.inputWrapper}>
            <input
              type="text"
              autoFocus
              placeholder="Fill in the title"
              className={style.input}
              value={localTitle}
              onChange={handleChange}
              onKeyUp={handleKeys}
            />
          </span>
          <button title="Save task" onClick={handleSave}>
            ✔
          </button>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <span
            className={cx(style.title, isCompleted && style.completed)}
            onClick={handleEditRequest}
            title="Click to edit"
          >
            {title}
          </span>
          <button title="Delete task" onClick={onDelete}>
            ✖
          </button>
        </React.Fragment>
      )}
    </div>
  );
};

export default Task;
