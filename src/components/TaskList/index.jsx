import React from 'react';
import cx from 'classnames';
import Task from '../Task';
import style from './style.module.css';

const TaskList = ({ tasks, onDelete, onStatusChange, onEdit, completed }) => {
  const filteredTasks = tasks.filter(
    ({ isCompleted }) => isCompleted === completed
  );
  return (
    <div className={style.container}>
      <header
        className={cx(
          style.header,
          completed ? style.completed : style.notCompleted
        )}
      >
        {completed ? 'Completed' : 'Not completed'}
      </header>
      {filteredTasks.length === 0 ? (
        <span className={style.emptyList}>Empty here</span>
      ) : (
        filteredTasks.map(({ id, title, isCompleted }) => (
          <Task
            key={id}
            title={title}
            isCompleted={isCompleted}
            onStatusChange={onStatusChange(id)}
            onDelete={onDelete(id)}
            onEdit={onEdit(id)}
          />
        ))
      )}
    </div>
  );
};

export default TaskList;
