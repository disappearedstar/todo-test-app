import React from 'react';
import TaskContainer from '../../containers/TaskContainer';
import style from './style.module.css';

function App() {
  return (
    <div className={style.container}>
      <TaskContainer />
    </div>
  );
}

export default App;
