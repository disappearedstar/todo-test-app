import React, { useState, useCallback } from 'react';
import { useKeyPress } from '../../utils/customHooks';
import { KEY_ENTER } from '../../utils/constants';
import style from './style.module.css';

const NewTask = ({ onAdd }) => {
  const empty = '';
  const [title, setTitle] = useState(empty);

  const handleChange = useCallback(
    ({ target: { value } }) => setTitle(value),
    []
  );

  const handleAdd = useCallback(() => {
    onAdd(title);
    setTitle(empty);
  }, [title, onAdd]);

  const handleEnter = useKeyPress({ [KEY_ENTER]: handleAdd });

  return (
    <div className={style.container}>
      <header>Add task:</header>
      <div className={style.controls}>
        <input
          type="text"
          placeholder="Task title"
          autoFocus
          value={title}
          onChange={handleChange}
          onKeyUp={handleEnter}
        />
        <button onClick={handleAdd}>Add</button>
      </div>
    </div>
  );
};

export default NewTask;
