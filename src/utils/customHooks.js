import { useCallback } from 'react';

/**
 * @param {{[keyCode: number]: Function}} keyHandlerMapping Key code => callback mapping
 */
export const useKeyPress = keyHandlerMapping =>
  useCallback(
    ({ which }) => {
      if (keyHandlerMapping[which]) {
        keyHandlerMapping[which]();
      }
    },
    [keyHandlerMapping]
  );
