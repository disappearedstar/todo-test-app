import Ajv from 'ajv';
import uuidv4 from 'uuid/v4';
import schema from '../../todos-schema.json';
import { LocalStorage } from '../../utils/storage';

const ajv = new Ajv();
const validate = ajv.compile(schema);

const TASKS_STORAGE_KEY = 'tasks';

/**
 * Sort tasks by their title in reverse (descending) order
 * @param {Array<{id: string, title: string, isCompleted: boolean}>} tasks
 */
export const sortTasks = tasks =>
  tasks.length < 2
    ? tasks
    : tasks.sort(({ title: a }, { title: b }) =>
        a === b ? 0 : a < b ? 1 : -1
      );

/**
 * @param {Array<{id: string, title: string, isCompleted: boolean}>} tasks
 * @param {string} taskId
 * @param {Function} callbackFn
 */
export const manipulateTaskById = (tasks, taskId, callbackFn) => {
  const index = tasks.findIndex(({ id }) => id === taskId);
  if (index !== -1) {
    return callbackFn(index);
  }
  return null;
};

export const retrieveTasksFromPersistentStorage = () =>
  LocalStorage.getItem(TASKS_STORAGE_KEY);

export const saveTasksToPersistentStorage = tasks =>
  LocalStorage.setItem(TASKS_STORAGE_KEY, JSON.stringify(tasks));

export const getValidTasks = rawTasks => {
  if (!rawTasks) {
    return [];
  }

  try {
    const tasks = JSON.parse(rawTasks);
    if (validate(tasks)) {
      return tasks;
    }
    return [];
  } catch (err) {
    return [];
  }
};

export const getUniqueTaskId = () => uuidv4();
