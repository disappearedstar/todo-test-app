import React, { PureComponent } from 'react';
import NewTask from '../../components/NewTask';
import TaskList from '../../components/TaskList';
import {
  getValidTasks,
  getUniqueTaskId,
  sortTasks,
  retrieveTasksFromPersistentStorage,
  saveTasksToPersistentStorage,
  manipulateTaskById,
} from './helpers';

class TaskContainer extends PureComponent {
  constructor(props) {
    super(props);
    const tasks = getValidTasks(retrieveTasksFromPersistentStorage());
    this.state = {
      tasks: sortTasks(tasks),
    };
  }

  componentDidUpdate() {
    saveTasksToPersistentStorage(this.state.tasks);
  }

  onAdd = taskTitle => {
    if (!taskTitle) {
      return;
    }
    this.setState(prevState => ({
      tasks: sortTasks(
        prevState.tasks.concat({
          id: getUniqueTaskId(),
          title: taskTitle,
          isCompleted: false,
        })
      ),
    }));
  };

  onDelete = taskId => () => {
    this.setState(prevState =>
      manipulateTaskById(prevState.tasks, taskId, index => ({
        tasks: [
          ...prevState.tasks.slice(0, index),
          ...prevState.tasks.slice(index + 1),
        ],
      }))
    );
  };

  onStatusChange = taskId => () => {
    this.setState(prevState =>
      manipulateTaskById(prevState.tasks, taskId, index => ({
        tasks: [
          ...prevState.tasks.slice(0, index),
          {
            ...prevState.tasks[index],
            isCompleted: !prevState.tasks[index].isCompleted,
          },
          ...prevState.tasks.slice(index + 1),
        ],
      }))
    );
  };

  onEdit = taskId => taskTitle => {
    this.setState(prevState =>
      manipulateTaskById(prevState.tasks, taskId, index => ({
        tasks: sortTasks([
          ...prevState.tasks.slice(0, index),
          {
            ...prevState.tasks[index],
            title: taskTitle,
          },
          ...prevState.tasks.slice(index + 1),
        ]),
      }))
    );
  };

  render() {
    const { tasks } = this.state;
    const handlers = {
      onDelete: this.onDelete,
      onStatusChange: this.onStatusChange,
      onEdit: this.onEdit,
    };
    return (
      <React.Fragment>
        <NewTask onAdd={this.onAdd} />
        <TaskList tasks={tasks} {...handlers} completed={false} />
        <TaskList tasks={tasks} {...handlers} completed={true} />
      </React.Fragment>
    );
  }
}

export default TaskContainer;
