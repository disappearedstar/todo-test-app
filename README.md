Todo App в качестве тестового задания.

Локальный запуск:
```sh
git clone https://gitlab.com/disappearedstar/todo-test-app.git
cd todo-test-app
npm install
npm start
```

Выложенное приложение можно посмотреть на https://disappearedstar.gitlab.io/todo-test-app (если 404, то это гитлаб тормозной).

Так как основной целью было поставлено time to market, разработка велась в следующих условиях:

* Для быстрого разворачивания рабочего окружения и сборки использован Create React App
* Отсутствуют тесты и проверки prop types, однако есть линтинг
* Узкий круг браузеров, заявленных в качестве поддерживаемых, по факту работа в них не проверялась
* Нет никакого дизайна, простейшая верстка в колонку на флексах, без сторонних библиотек UI-компонентов
* Не оптимизирован рендеринг (весьма вероятно, что у некоторых компонентов есть лишние циклы обновления)
* В качестве персистентного хранилища используется только local storage без фоллбека на другие хранилища (cookies, indexedDB) в случае недоступности

Примечания:

* Код самого приложения не разбит на коммиты. Сначала не увидел в этом смысла на этапе создания минимально работающего приложения, а потом не стал тратить время на ребейзы.
* Не стал завозить Redux в такое маленькое приложение, поэтому использовал локальный стейт и React-хуки.
* Добавил валидацию сохраняемого состояния по схеме, чтобы приложение не ломалось, если вручную отредактировать local storage.
* Настроил простой пайплайн в GitLab: линтинг -> сборка -> выкладывание на Pages.
